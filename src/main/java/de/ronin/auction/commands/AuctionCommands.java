package de.ronin.auction.commands;

import de.ostylk.baseapi.modules.command.Command;
import de.ostylk.baseapi.modules.command.Optional;
import de.ronin.auction.RoninAuction;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

public class AuctionCommands {
    
    @Command("auction.npc.spawn")
    public void spawnNPC(Player player) {
        NPC npc = CitizensAPI.getNPCRegistry().createNPC(EntityType.PLAYER, "\u00a76Auktionator");
        npc.spawn(player.getLocation());
        RoninAuction.getInstance().getNPCCache().addNPC(npc);
        player.sendMessage("\u00a7aAuktionator NPC erfolgreich gespawnt.");
    }
    
    @Command("auction.npc.remove")
    public void removeNPC(Player player, @Optional("$selected") NPC npc) {
        npc.destroy();
        RoninAuction.getInstance().getNPCCache().removeNPC(npc);
        player.sendMessage("\u00a7aAuktionator gelöscht.");
    }
}
