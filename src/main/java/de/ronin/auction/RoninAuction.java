package de.ronin.auction;

import de.ostylk.baseapi.BaseAPI;
import de.ostylk.baseapi.modules.command.ModuleCommand;
import de.ostylk.baseapi.modules.config.ModuleConfig;
import de.ostylk.baseapi.modules.config.settings.SettingManager;
import de.ostylk.baseapi.modules.economy.Currency;
import de.ostylk.baseapi.modules.economy.ModuleEconomy;
import de.ostylk.baseapi.modules.item.ModuleItem;
import de.ostylk.baseapi.modules.playercache.ModulePlayerCache;
import de.ostylk.baseapi.modules.window.ModuleWindow;
import de.ronin.auction.auction.big.BigAuction;
import de.ronin.auction.auction.big.BigAuctionCache;
import de.ronin.auction.auction.normal.NormalAuction;
import de.ronin.auction.auction.normal.NormalAuctionCache;
import de.ronin.auction.auction.special.SpecialAuction;
import de.ronin.auction.auction.special.SpecialAuctionCache;
import de.ronin.auction.listener.AuctioneerMainMenu;
import de.ronin.auction.npc.AuctionNPCCache;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.concurrent.Executor;

public class RoninAuction extends JavaPlugin {

    public static Executor BUKKIT_EXEC;
    
    public static ModuleCommand COMMAND;
    public static ModuleConfig CONFIG;
    public static ModuleEconomy ECONOMY;
    public static ModuleItem ITEM;
    public static ModulePlayerCache PLAYER_CACHE;
    public static ModuleWindow WINDOW;
    
    public static Currency CURRENCY_MAIN;
    
    private AuctionNPCCache npcCache;
    private NormalAuctionCache normalAuctionCache;
    private SpecialAuctionCache specialAuctionCache;
    private BigAuctionCache bigAuctionCache;
    
    @Override
    public void onLoad() {
        BaseAPI baseAPI = Bukkit.getServicesManager().getRegistration(BaseAPI.class).getProvider();
        BUKKIT_EXEC = baseAPI.bukkitExecutor();
        
        COMMAND = baseAPI.getModule(ModuleCommand.class);
        CONFIG = baseAPI.getModule(ModuleConfig.class);
        ECONOMY = baseAPI.getModule(ModuleEconomy.class);
        ITEM = baseAPI.getModule(ModuleItem.class);
        PLAYER_CACHE = baseAPI.getModule(ModulePlayerCache.class);
        WINDOW = baseAPI.getModule(ModuleWindow.class);
    
        SettingManager settings = CONFIG.getSettingManager(this);
        settings.loadSettings(NormalAuction.class);
        settings.loadSettings(SpecialAuction.class);
        settings.loadSettings(BigAuction.class);
        
        COMMAND.scanPlugin(this);
    }

    @Override
    public void onEnable() {
        CURRENCY_MAIN = ECONOMY.getCurrency("ronin_main");
        
        this.npcCache = new AuctionNPCCache(CONFIG.loadConfig(this, "auction_npcs"));
        
        this.normalAuctionCache = new NormalAuctionCache(CONFIG.loadConfig(this, "auctions/normal"));
        this.specialAuctionCache = new SpecialAuctionCache(CONFIG.loadConfig(this, "auctions/special"));
        this.bigAuctionCache = new BigAuctionCache(CONFIG.loadConfig(this, "auctions/big"));
        getServer().getScheduler().runTaskTimer(this, this.normalAuctionCache, 20L * 60L, 20L * 60L);
        getServer().getScheduler().runTaskTimer(this, this.specialAuctionCache, 20L * 60L, 20L * 60L);
        getServer().getScheduler().runTaskTimer(this, this.bigAuctionCache, 20L * 60L, 20L * 60L);
        
        getServer().getPluginManager().registerEvents(new AuctioneerMainMenu(), this);
        
        COMMAND.finishPlugin(this, "roninauction.command");
    }

    @Override
    public void onDisable() {

    }
    
    public AuctionNPCCache getNPCCache() {
        return this.npcCache;
    }
    
    public NormalAuctionCache getNormalAuctionCache() {
        return this.normalAuctionCache;
    }
    
    public SpecialAuctionCache getSpecialAuctionCache() {
        return this.specialAuctionCache;
    }
    
    public BigAuctionCache getBigAuctionCache() {
        return this.bigAuctionCache;
    }
    
    public static RoninAuction getInstance() {
        return RoninAuction.getPlugin(RoninAuction.class);
    }
}
