package de.ronin.auction.npc;

import de.ostylk.baseapi.modules.config.Config;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.entity.Entity;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class AuctionNPCCache {

    private Config config;
    private Set<Integer> npcs;
    
    public AuctionNPCCache(Config config) {
        this.config = config;
        this.npcs = new HashSet<>();
        
        if (this.config.contains("npcs")) {
            this.npcs = new HashSet<>(this.config.getIntegerList("npcs"));
        }
    }
    
    private void save() {
        this.config.set("npcs", new ArrayList<>(this.npcs));
        this.config.save();
    }
    
    public void addNPC(NPC npc) {
        this.npcs.add(npc.getId());
        this.save();
    }
    
    public void removeNPC(NPC npc) {
        this.npcs.remove(npc.getId());
        this.save();
    }
    
    public boolean isNPC(Entity entity) {
        if (entity.hasMetadata("NPC")) {
            NPC npc = CitizensAPI.getNPCRegistry().getNPC(entity);
            return this.npcs.contains(npc.getId());
        }
        
        return false;
    }
}
