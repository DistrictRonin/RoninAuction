package de.ronin.auction.auction;

import de.ostylk.baseapi.modules.window.Window;
import de.ostylk.baseapi.modules.window.WindowInstance;
import de.ostylk.baseapi.modules.window.node.NodeContainer;
import de.ostylk.baseapi.modules.window.node.elements.Button;
import de.ostylk.baseapi.modules.window.player.PlayerContext;
import de.ronin.auction.RoninAuction;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;

public class AuctionCancel {

    private Window<NodeContainer> root;
    
    private static final String PROP_AUCTION = "auction";
    
    public AuctionCancel() {
        this.root = RoninAuction.WINDOW.createNodeWindow(9, 1, false);
        Button cancel = this.root.getContainer().nodeCreator().button(7, 0,
                RoninAuction.ITEM.builder(Material.RED_TERRACOTTA)
                        .name("\u00a7cAuktion abbrechen")
                        .addLore("\u00a77Klicke hier, um die Auktion abzubrechen.")
                        .addLore("\u00a77Du kriegst die noch übrigen Items wieder.")
                .finish()
        );
        cancel.registerListener(instance -> {
            Auction auction = (Auction) instance.getProperty(PROP_AUCTION, null);
            PlayerContext context = RoninAuction.WINDOW.getContext(instance.getPlayer());
            context.openConfirmation("Auktion abbrechen", player -> {
                auction.cancel();
                player.sendMessage("\u00a7cAuktion abgebrochen.");
                Bukkit.getScheduler().runTaskLater(RoninAuction.getInstance(), context::close, 3L);
            }, "Auktion nicht abbrechen", player -> {}, auction.makeOverview());
        });
    }
    
    public void open(Player player, Auction auction) {
        WindowInstance<NodeContainer> instance = RoninAuction.WINDOW.getContext(player).open(this.root, InventoryType.CHEST, "\u00a74Auktion abbrechen");
        instance.getContainer().nodeCreator().node(1, 0, auction.makeOverview());
        instance.setProperty(PROP_AUCTION, auction);
    }
}
