package de.ronin.auction.auction.normal;

import de.ostylk.baseapi.modules.config.Config;
import de.ronin.auction.auction.AuctionCache;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

public class NormalAuctionCache extends AuctionCache<NormalAuction> {
    
    private NormalAuctionCreateWindow createWindow;
    
    public NormalAuctionCache(Config config) {
        super(config);
        this.createWindow = new NormalAuctionCreateWindow();
    }
    
    @Override
    public NormalAuction loadEntry(String key) {
        UUID player = UUID.fromString(this.config.getString(key + ".player"));
        ItemStack itemType = this.config.getItemStack(key + ".item");
        int price = this.config.getInt(key + ".price");
        long amount = this.config.getLong(key + ".amount");
        long deadline = this.config.getLong(key + ".deadline");
        
        return new NormalAuction(player, itemType, price, amount, deadline);
    }
    
    @Override
    public void saveEntry(String key, NormalAuction auction) {
        this.config.set(key + ".player", auction.getPlayer().toString());
        this.config.set(key + ".item", auction.getItemType());
        this.config.set(key + ".price", auction.getPrice());
        this.config.set(key + ".amount", auction.getAmount());
        this.config.set(key + ".deadline", auction.getDeadline());
    }
    
    public NormalAuctionCreateWindow getCreateWindow() {
        return this.createWindow;
    }
}
