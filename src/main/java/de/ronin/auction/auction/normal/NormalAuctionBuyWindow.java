package de.ronin.auction.auction.normal;

import de.ostylk.baseapi.modules.economy.Account;
import de.ostylk.baseapi.modules.window.Window;
import de.ostylk.baseapi.modules.window.WindowInstance;
import de.ostylk.baseapi.modules.window.node.Node;
import de.ostylk.baseapi.modules.window.node.NodeContainer;
import de.ostylk.baseapi.modules.window.node.elements.Button;
import de.ostylk.baseapi.modules.window.node.elements.ValueChanger;
import de.ronin.auction.ItemUtil;
import de.ronin.auction.RoninAuction;
import de.ronin.post.PostAPI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class NormalAuctionBuyWindow {
    
    private static final String ND_OVERVIEW = "nd_overview";
    private static final String VAL_AMOUNT = "val_amount";
    private static final String BTN_BUY = "btn_buy";
    private static final String BTN_BUY_ALL = "btn_buy_all";
    
    private static final String CONT_BUTTONS = "cont_buttons";
    
    private NormalAuction auction;
    private Window<NodeContainer> root;
    
    public NormalAuctionBuyWindow(NormalAuction auction) {
        this.auction = auction;
        this.root = RoninAuction.WINDOW.createNodeWindow(9, 1, true);
        this.root.getContainer().nodeCreator().node(0, 0, new ItemStack(Material.STONE), ND_OVERVIEW);
        this.update();
        
        Window<NodeContainer> buttons = RoninAuction.WINDOW.createNodeWindow(8, 1, false);
        buttons.getContainer().nodeCreator().valueChanger(1, 0, VAL_AMOUNT)
                .min(0)
                .max((int) this.auction.getAmount())
                .itemStyler((meta, current, amount) -> {
                    meta.setDisplayName("\u00a7eMomentane Anzahl: \u00a77" + ItemUtil.nameQuantity(current));
                    List<String> lore = new ArrayList<>();
                    lore.add("\u00a7bLinksklick um mehr zu kaufen (" + amount + ")");
                    lore.add("\u00a7cRechtsklick um weniger zu kaufen (" + amount + ")");
                    meta.setLore(lore);
                    return meta;
                })
                .addPair(Material.IRON_INGOT, 1)
                .addPair(Material.GOLD_INGOT, 16)
                .addPair(Material.DIAMOND, 64)
                .addPair(Material.DIAMOND_BLOCK, 576)
                .finish()
        .registerChangeListener((root, instance, value) -> this.updateButtons(instance));
        
        Button buy = buttons.getContainer().nodeCreator().button(7, 0, this.makeBuyButton(0), BTN_BUY);
        buy.registerListener(instance -> {
            ValueChanger amount = instance.getContainer().getMultiNode(VAL_AMOUNT);
            this.buy(instance.getPlayer(), amount.getValue());
        });
        Button buyAll = buttons.getContainer().nodeCreator().button(6, 0, this.makeBuyAllButton(), BTN_BUY_ALL);
        buyAll.registerListener(instance -> {
            this.buy(instance.getPlayer(), this.auction.getAmount());
        });
        
        this.root.addChild(buttons, 1, 0, CONT_BUTTONS);
    }
    
    private boolean addWithOverflow(Player player, ItemStack... items) {
        Map<Integer, ItemStack> leftover = player.getInventory().addItem(items);
        if (!leftover.isEmpty()) {
            leftover.values().forEach(is -> PostAPI.sendItems(player.getUniqueId(), is));
            return true;
        }
        return false;
    }
    
    private void buy(Player player, long amount) {
        if (amount <= 0) return;
        
        CompletableFuture<Account> buyerF = RoninAuction.ECONOMY.getAccountAsync(player.getUniqueId(), RoninAuction.CURRENCY_MAIN);
        CompletableFuture<Account> ownerF = RoninAuction.ECONOMY.getAccountAsync(this.auction.getPlayer(), RoninAuction.CURRENCY_MAIN);
        buyerF.thenAcceptBothAsync(ownerF, (buyer, owner) -> {
            long total = amount * this.auction.getPrice();
            if (buyer.getBalance() < total) {
                player.sendMessage("\u00a7cDu hast nicht genug Geld.");
                return;
            }
            
            long actualAmount = this.auction.tryTakeAmount(amount);
            PostAPI.safeTransferMoney(buyer, owner, actualAmount * this.auction.getPrice());
            
            Player ownerPlayer = Bukkit.getPlayer(this.auction.getPlayer());
            if (ownerPlayer != null) {
                ownerPlayer.sendMessage("\u00a7aEs wurden " + actualAmount + " " + this.auction.getItemType().getItemMeta().getDisplayName() + " für " +
                        (actualAmount * this.auction.getPrice()) + " " + RoninAuction.CURRENCY_MAIN.getName() + " gekauft.");
            }
    
            if (this.addWithOverflow(player, ItemUtil.amountToItemStacks(this.auction.getItemType(), (int) actualAmount))) {
                player.sendMessage("\u00a7aDein Inventar ist voll. Die restlichen Items wurden in dein Postfach gelegt!");
            }
            player.sendMessage("\u00a7aDu hast " + actualAmount + " Items gekauft.");
        }).thenRunAsync(() -> {
            RoninAuction.WINDOW.getContext(player).close();
        }, RoninAuction.BUKKIT_EXEC);
    }
    
    private void updateButtons(WindowInstance<NodeContainer> instance) {
        ValueChanger amount = instance.getContainer().getMultiNode(VAL_AMOUNT);
        amount.setMax((int) this.auction.getAmount());
        
        Button buy = instance.getContainer().getNode(BTN_BUY);
        buy.setIcon(this.makeBuyButton(amount.getValue()));
        
        Button buyAll = instance.getContainer().getNode(BTN_BUY_ALL);
        buyAll.setIcon(this.makeBuyAllButton());
    }
    
    
    private ItemStack makeBuyButton(int amount) {
        return RoninAuction.ITEM.builder(Material.GREEN_TERRACOTTA)
                .name("\u00a72" + ItemUtil.nameQuantity(amount) + " kaufen")
                .addLore("\u00a7eKosten: " + (amount * this.auction.getPrice()) + " " + RoninAuction.CURRENCY_MAIN.getName())
                .finish();
    }
    
    private ItemStack makeBuyAllButton() {
        return RoninAuction.ITEM.builder(Material.GREEN_GLAZED_TERRACOTTA)
                .name("\u00a72" + "Alles kaufen")
                .addLore("\u00a7eAnzahl: \u00a77" + ItemUtil.nameQuantity(this.auction.getAmount()))
                .addLore("\u00a7eKosten: \u00a77" + (this.auction.getAmount() * this.auction.getPrice()) + " " + RoninAuction.CURRENCY_MAIN.getName())
                .finish();
    }
    
    protected void update() {
        Node node = this.root.getContainer().getNode(ND_OVERVIEW);
        node.setIcon(this.auction.makeOverview());
    }
    
    public void open(Player player) {
        RoninAuction.WINDOW.getContext(player).open(this.root, InventoryType.CHEST, "\u00a72Items kaufen");
    }
}
