package de.ronin.auction.auction.normal;

import com.google.common.collect.Lists;
import de.ostylk.baseapi.modules.config.settings.SettingLoad;
import de.ostylk.baseapi.modules.item.ItemBuilder;
import de.ronin.auction.ItemUtil;
import de.ronin.auction.RoninAuction;
import de.ronin.auction.auction.Auction;
import de.ronin.post.PostAPI;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class NormalAuction extends Auction {
    
    @SettingLoad("auction_normal.tax")
    public static List<Double> TAX = Lists.newArrayList(0.05, 0.06, 0.07, 0.08, 0.09);
    @SettingLoad("auction_normal.duration")
    public static List<Integer> DURATION = Lists.newArrayList(1, 2, 3, 4, 5);
    @SettingLoad("auction_normal.max_price")
    public static int MAX_PRICE = 1000;
    
    private ItemStack itemType;
    private long amount;
    private int price;
    
    private NormalAuctionBuyWindow buyWindow;
    
    public NormalAuction(UUID player, ItemStack itemType, int price, long amount, long deadline) {
        super(player, deadline);
        
        this.itemType = itemType;
        this.itemType.setAmount(1);
        this.amount = amount;
        this.price = price;
        
        this.buyWindow = new NormalAuctionBuyWindow(this);
    }
    
    @Override
    public ItemStack makeOverview() {
        String name = this.getPlayer().toString();
        try {
            name = RoninAuction.PLAYER_CACHE.requestPlayerNameAsync(this.getPlayer()).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    
        ItemBuilder overview = RoninAuction.ITEM.builder(this.itemType)
                .addLore("\u00a7eVerkäufer: \u00a77" + name)
                .addLore("\u00a7eStückpreis: \u00a77" + this.price + " " + RoninAuction.CURRENCY_MAIN.getName())
                .addLore("\u00a7eAnzahl: \u00a77" + ItemUtil.nameQuantity(this.amount))
                .addLore("\u00a7eGesamtpreis: \u00a77" + (this.price * this.amount) + " " + RoninAuction.CURRENCY_MAIN.getName());
        return overview.finish();
    }
    
    @Override
    public void openBuyMenu(Player player) {
        this.buyWindow.open(player);
    }
    
    @Override
    public void cancel() {
        if (this.amount <= 0) return;
        if (this.itemType == null) return;
        
        PostAPI.sendItems(this.getPlayer(), ItemUtil.amountToItemStacks(this.itemType, (int) this.amount));
        
        RoninAuction.getInstance().getNormalAuctionCache().remove(this);
        RoninAuction.getInstance().getNormalAuctionCache().save();
        
        // save precautions to avoid item duplication
        this.amount = 0;
        this.buyWindow.update();
        
        //TODO: offline messages :)
    }
    
    protected long tryTakeAmount(long amount) {
        if (this.amount > amount) {
            this.amount -= amount;
            this.buyWindow.update();
            
            RoninAuction.getInstance().getNormalAuctionCache().save();
            
            return amount;
        } else {
            RoninAuction.getInstance().getNormalAuctionCache().remove(this);
            RoninAuction.getInstance().getNormalAuctionCache().save();
            
            amount = this.amount;
            this.amount = 0;
            this.buyWindow.update();
            
            return amount;
        }
    }
    
    public ItemStack getItemType() {
        return this.itemType;
    }
    
    public long getAmount() {
        return this.amount;
    }
    
    public int getPrice() {
        return this.price;
    }
}
