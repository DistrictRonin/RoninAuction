package de.ronin.auction.auction.normal;

import de.ostylk.baseapi.modules.economy.Account;
import de.ostylk.baseapi.modules.window.Container;
import de.ostylk.baseapi.modules.window.ModuleWindow;
import de.ostylk.baseapi.modules.window.Window;
import de.ostylk.baseapi.modules.window.WindowInstance;
import de.ostylk.baseapi.modules.window.WindowInstance.WindowInstanceChild;
import de.ostylk.baseapi.modules.window.node.NodeContainer;
import de.ostylk.baseapi.modules.window.node.elements.Button;
import de.ostylk.baseapi.modules.window.node.elements.ValueChanger;
import de.ostylk.baseapi.modules.window.player.PlayerContext;
import de.ostylk.baseapi.modules.window.storage.FilterResult;
import de.ostylk.baseapi.modules.window.storage.StorageContainer;
import de.ostylk.baseapi.modules.window.storage.event.FilterEvent;
import de.ostylk.baseapi.modules.window.storage.event.FilterEventType;
import de.ronin.auction.RoninAuction;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

public class NormalAuctionCreateWindow {
    
    private Window<NodeContainer> root;
    
    private static final String CONT_CONTENT = "cont_content";
    
    private static final String VAL_PRICE = "val_price";
    private static final String BTN_DURATION = "btn_duration";
    private static final String BTN_FINISH = "btn_finish";
    
    private static final String PROP_DURATION = "duration";
    
    public NormalAuctionCreateWindow() {
        ModuleWindow win = RoninAuction.WINDOW;
        
        this.root = win.createNodeWindow(9, 5, false);
        
        Window<StorageContainer> auctionContent = win.createStorageWindow(9, 3, false);
        auctionContent.getContainer().registerFilter(this::filterContent);
        auctionContent.getContainer().registerInvalidationListener((root, instance) -> {
            if (instance != null) {
                //noinspection unchecked
                this.updateButtons((WindowInstance<NodeContainer>) root);
            }
        });
        auctionContent.registerCloseListener((root, instance, type) -> {
            if (type != Window.CloseType.POP) return;
            // Players shouldn't lose their items
            for (ItemStack is : instance.getContainer().getContents()) {
                if (is == null) continue;
                instance.getPlayer().getInventory().addItem(is);
            }
        });
        this.root.addChild(auctionContent, 0, 0, CONT_CONTENT);
        this.root.setChildMoveHandler(CONT_CONTENT);
    
        this.root.getContainer().nodeCreator().valueChanger(1, 4, VAL_PRICE)
                .min(1)
                .value(1)
                .max(NormalAuction.MAX_PRICE)
                .itemStyler((meta, current, amount) -> {
                    meta.setDisplayName("\u00a7eMomentaner Stückpreis: \u00a77" + current + " " + RoninAuction.CURRENCY_MAIN.getName());
                    List<String> lore = new ArrayList<>();
                    lore.add("\u00a7bLinksklick um den Stückpreis zu erhöhen (" + amount + ")");
                    lore.add("\u00a7cRechtsklick um den Stückpreis zu senken (" + amount + ")");
                    meta.setLore(lore);
                    return meta;
                })
                .addPair(Material.IRON_INGOT, 1)
                .addPair(Material.GOLD_INGOT, 10)
                .addPair(Material.DIAMOND, 100)
                .finish()
        .registerChangeListener((root, instance, value) -> this.updateButtons(instance));
        
        Button duration = this.root.getContainer().nodeCreator().button(6, 4, this.makeDurationButton(0), BTN_DURATION);
        duration.registerExtendedListener((root, instance, info) -> this.modifyDuration(instance, info));
        
        Button finish = this.root.getContainer().nodeCreator().button(8, 4, this.makeFinishButton(0, 0, null), BTN_FINISH);
        finish.registerListener(this::finishAuction);
    }
    
    private void finishAuction(WindowInstance<NodeContainer> instance) {
        WindowInstanceChild<StorageContainer> content = instance.searchChild(CONT_CONTENT);
        ValueChanger price = instance.getContainer().getMultiNode(VAL_PRICE);
        int amount = content.child.getContainer().count();
        int total = price.getValue() * amount;
        int duration = (int) instance.getProperty(PROP_DURATION, 0);
        double tax = NormalAuction.TAX.get(duration);
        PlayerContext context = RoninAuction.WINDOW.getContext(instance.getPlayer());
        ItemStack firstItem = Arrays.stream(content.child.getContainer().getContents())
                .filter(Objects::nonNull)
                .filter(is -> is.getType() != Material.AIR)
                .findAny().orElse(null);
        
        if (firstItem == null) return;
        
        ItemStack type = firstItem.clone();
        type.setAmount(1);
        ItemStack info = RoninAuction.ITEM.builder(type)
                .setAmount(1)
                .addLore("\u00a77Stückpreis: " + price.getValue() + " " + RoninAuction.CURRENCY_MAIN.getName())
                .addLore("\u00a77Anzahl: " + amount)
                .addLore("\u00a77Gesamtpreis: " + total + " " + RoninAuction.CURRENCY_MAIN.getName())
                .addLore(" ")
                .addLore("\u00a77Gebühr: " + String.format("%.2f %s(%.2f%s)", total * tax, RoninAuction.CURRENCY_MAIN.getName(), tax * 100.0, "%"))
                .addLore("\u00a77Dauer: " + NormalAuction.DURATION.get(duration) + " Tage")
                .finish();
        
        Bukkit.getScheduler().runTaskLater(RoninAuction.getInstance(), () -> {
            context.openConfirmation("Auktion erstellen", player -> {
                if (this.makeFinalAuction(instance.getPlayer(), type, amount, price.getValue(), duration)) {
                    content.child.getContainer().clear();
                    Bukkit.getScheduler().runTaskLater(RoninAuction.getInstance(), context::close, 3L);
                }
            }, "Abbrechen!", player -> {}, info);
        }, 1L);
    }
    
    private boolean makeFinalAuction(Player seller, ItemStack type, int amount, int price, int duration) {
        int total = amount * price;
        double tax = NormalAuction.TAX.get(duration) * total;
        long durationInMillis = (long) (NormalAuction.DURATION.get(duration)) * 24L * 60L * 60L * 1000L;
    
        Account account;
        try {
            account = RoninAuction.ECONOMY.getAccountAsync(seller.getUniqueId(), RoninAuction.CURRENCY_MAIN).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return false;
        }
        
        switch (account.withdraw(tax)) {
            case SUCCESS:
                NormalAuction auction = new NormalAuction(
                        seller.getUniqueId(),
                        type,
                        price,
                        amount,
                        System.currentTimeMillis() + durationInMillis
                );
                RoninAuction.getInstance().getNormalAuctionCache().add(auction);
                RoninAuction.getInstance().getNormalAuctionCache().save();
                
                seller.sendMessage("\u00a7aAuktion erfolgreich erstellt!");
                return true;
            case NOT_ENOUGH_MONEY:
                seller.sendMessage("\u00a7cDu hast nicht genug " + RoninAuction.CURRENCY_MAIN.getName() + ", um die Gebühren zu bezahlen!");
                return false;
        }
        return false;
    }
    
    private void modifyDuration(WindowInstance<NodeContainer> instance, Button.ClickInfo info) {
        int duration = (int) instance.getProperty(PROP_DURATION, 0);
        if (info.isRight()) {
            duration = Math.max(0, duration - 1);
        } else {
            duration = Math.min(NormalAuction.DURATION.size() - 1, duration + 1);
        }
        instance.setProperty(PROP_DURATION, duration);
        
        this.updateButtons(instance);
    }
    
    private FilterResult filterContent(WindowInstance<? extends Container> root, WindowInstance<StorageContainer> instance, FilterEventType type, FilterEvent event) {
        for (int i = 0; i < instance.getContainer().getContents().length; i++) {
            ItemStack is = instance.getContainer().get(i);
            if (is != null && is.getType() != Material.AIR) {
                if (!event.getItem().isSimilar(is)) {
                    return FilterResult.BLOCK;
                }
            }
        }
        return FilterResult.ALLOW;
    }
    
    private void updateButtons(WindowInstance<NodeContainer> instance) {
        ValueChanger price = instance.getContainer().getMultiNode(VAL_PRICE);
        int duration = (int) instance.getProperty(PROP_DURATION, 0);
        WindowInstanceChild<StorageContainer> content = instance.searchChild(CONT_CONTENT);
    
        Button durationBtn = instance.getContainer().getNode(BTN_DURATION);
        durationBtn.setIcon(this.makeDurationButton(duration));
    
        Button finish = instance.getContainer().getNode(BTN_FINISH);
        finish.setIcon(this.makeFinishButton(price.getValue(), duration, content.child.getContainer()));
    }
    
    private ItemStack makeDurationButton(int duration) {
        return RoninAuction.ITEM.builder(Material.STICK)
                .name("\u00a7eMomentane Auftragsdauer: \u00a77" + NormalAuction.DURATION.get(duration) + " Tage")
                .addLore("\u00a7bLinksklick um die Auftragsdauer zu verlängern")
                .addLore("\u00a7cRechtsklick um die Auftragsdauer zu verkürzen")
                .setAmount(NormalAuction.DURATION.get(duration))
                .finish();
    }
    
    private ItemStack makeFinishButton(int price, int duration, StorageContainer content) {
        int totalPrice = 0;
        if (content != null) {
            totalPrice = content.count() * price;
        }
        return RoninAuction.ITEM.builder(Material.GREEN_TERRACOTTA)
                .name("\u00a72Auktion erstellen")
                .addLore("\u00a77Stückpreis: " + price  + " " + RoninAuction.CURRENCY_MAIN.getName())
                .addLore("\u00a77Gesamtpreis: " + totalPrice + " " + RoninAuction.CURRENCY_MAIN.getName())
                .addLore("\u00a77Gebühr: " + String.format("%.2f%s", NormalAuction.TAX.get(duration) * 100, "%"))
                .addLore("\u00a77Dauer: " + NormalAuction.DURATION.get(duration) + " Tage")
                .finish();
    }
    
    public void open(Player player) {
        RoninAuction.WINDOW.getContext(player).open(this.root, InventoryType.CHEST, "\u00a72Auktion erstellen");
    }
}
