package de.ronin.auction.auction.big;

import com.google.common.collect.Lists;
import de.ostylk.baseapi.modules.config.settings.SettingLoad;
import de.ostylk.baseapi.modules.item.ItemBuilder;
import de.ronin.auction.ItemUtil;
import de.ronin.auction.RoninAuction;
import de.ronin.auction.auction.Auction;
import de.ronin.post.PostAPI;
import org.apache.commons.lang3.tuple.Pair;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class BigAuction extends Auction {
    
    @SettingLoad("auction_big.tax")
    public static List<Double> TAX = Lists.newArrayList(0.05, 0.06, 0.07);
    @SettingLoad("auction_big.duration")
    public static List<Integer> DURATION = Lists.newArrayList(7, 14, 21);
    
    private List<BigAuctionPart> parts;
    
    private BigAuctionDeliverWindow deliverWindow;
    
    public BigAuction(UUID player, long deadline, List<BigAuctionPart> parts) {
        super(player, deadline);
        
        this.parts = parts;
        this.deliverWindow = new BigAuctionDeliverWindow(this);
    }
    
    @Override
    public ItemStack makeOverview() {
        String name = this.getPlayer().toString();
        try {
            name = RoninAuction.PLAYER_CACHE.requestPlayerNameAsync(this.getPlayer()).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        ItemBuilder overviewBuilder = RoninAuction.ITEM.builder(Material.WRITABLE_BOOK)
                .name("\u00a7e" + name + "s Großauftrag");
    
        for (BigAuctionPart part : this.parts) {
            overviewBuilder = overviewBuilder.addLore("\u00a77" + part.getAmount() + "x " + part.getType().getType().name()
                    + " für " + String.format("%.2f", part.getBudget()) + " " + RoninAuction.CURRENCY_MAIN.getName());
        }
        return overviewBuilder.finish();
    }
    
    @Override
    public void openBuyMenu(Player player) {
        this.deliverWindow.open(player);
    }
    
    @Override
    public void cancel() {
        if (this.parts.size() <= 0) return;
    
        for (BigAuctionPart part : this.parts) {
            double budget = part.getBudget();
            RoninAuction.ECONOMY.getAccountAsync(this.getPlayer(), RoninAuction.CURRENCY_MAIN)
                    .thenAcceptAsync(acc -> PostAPI.safeDepositMoney(acc, budget));
        }
        
        RoninAuction.getInstance().getBigAuctionCache().remove(this);
        RoninAuction.getInstance().getBigAuctionCache().save();
    }
    
    private void sendMessageIfOnline(UUID player, String message) {
        Player p = Bukkit.getPlayer(player);
        if (p != null) {
            p.sendMessage(message);
        }
    }
    
    protected void deliverItems(List<Pair<BigAuctionPart, Integer>> contributions) {
        for (Pair<BigAuctionPart, Integer> contrib : contributions) {
            contrib.getLeft().takeAmount(contrib.getRight());
            
            this.sendMessageIfOnline(this.getPlayer(), "\u00a72Du hast " + contrib.getRight() + " " + contrib.getLeft().getType().getType().name() + " erhalten.");
            PostAPI.sendItems(this.getPlayer(), ItemUtil.amountToItemStacks(contrib.getLeft().getType(), contrib.getRight()));
        }
        
        int totalAmount = this.parts.stream().mapToInt(BigAuctionPart::getAmount).sum();
        if (totalAmount <= 0) {
            RoninAuction.getInstance().getBigAuctionCache().remove(this);
        }
        RoninAuction.getInstance().getBigAuctionCache().save();
        
        this.deliverWindow.update();
    }
    
    public List<BigAuctionPart> getParts() {
        return this.parts;
    }
}
