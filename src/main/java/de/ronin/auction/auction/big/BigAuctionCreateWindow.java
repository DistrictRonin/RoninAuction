package de.ronin.auction.auction.big;

import de.ostylk.baseapi.modules.item.ItemBuilder;
import de.ostylk.baseapi.modules.window.ModuleWindow;
import de.ostylk.baseapi.modules.window.Window;
import de.ostylk.baseapi.modules.window.WindowInstance;
import de.ostylk.baseapi.modules.window.WindowInstance.WindowInstanceChild;
import de.ostylk.baseapi.modules.window.misc.TabbedContainer;
import de.ostylk.baseapi.modules.window.node.NodeContainer;
import de.ostylk.baseapi.modules.window.node.elements.Button;
import de.ostylk.baseapi.modules.window.node.elements.ValueChanger;
import de.ostylk.baseapi.modules.window.player.PlayerContext;
import de.ostylk.baseapi.modules.window.storage.FilterResult;
import de.ostylk.baseapi.modules.window.storage.StorageContainer;
import de.ronin.auction.RoninAuction;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BigAuctionCreateWindow {
    
    private static final String CONT_TAB = "cont_tab";
    private static final String CONT_INPUT = "cont_input";
    
    private static final String[] CONT_PARTS = { "cont_1", "cont_2", "cont_3", "cont_4", "cont_5", "cont_6", "cont_7" };
    
    private static final String VAL_AMOUNT = "val_amount";
    private static final String VAL_BUDGET = "val_budget";
    
    private static final String BTN_DURATION = "btn_duration";
    private static final String BTN_FINISH = "btn_finish";
    
    private static final String PROP_DURATION = "prop_duration";
    
    private Window<NodeContainer> root;
    
    public BigAuctionCreateWindow() {
        ModuleWindow win = RoninAuction.WINDOW;
        
        this.root = win.createNodeWindow(9, 3, false);
        
        Window<TabbedContainer> tabbed = win.createTabbedWindow(7, 1, false);
        this.root.addChild(tabbed, 0, 0, CONT_TAB);
        
        Window<StorageContainer> input = win.createStorageWindow(1, 1, false);
        input.getContainer().registerFilter(((root, instance, type, event) -> {
            WindowInstance.WindowInstanceChild<TabbedContainer> tab = root.searchChild(CONT_TAB);
            if (tab.child.getContainer().getTabs().size() >= 7) {
                return FilterResult.BLOCK;
            }
            return FilterResult.ALLOW;
        }));
        input.getContainer().registerInvalidationListener((root, instance) -> {
            ItemStack is = instance.getContainer().get(0);
            if (is == null) return;
            is.clone();
            instance.getContainer().clear();
            instance.getPlayer().getInventory().addItem(is);
            
            is.setAmount(1);
            WindowInstance.WindowInstanceChild<TabbedContainer> tab = root.searchChild(CONT_TAB);
            int newID = tab.child.getContainer().getTabs().size();
            if (newID >= CONT_PARTS.length) return;
            
            tab.child.getContainer().addTab(newID, is, CONT_PARTS[newID]);
            //noinspection unchecked
            this.updateFinishButton((WindowInstance<NodeContainer>) root);
        });
        this.root.addChild(input, 7, 0, CONT_INPUT);
        this.root.setChildMoveHandler(CONT_INPUT);
        
        for (String contPart : CONT_PARTS) {
            Window<NodeContainer> window = this.makePartWindow();
            this.root.addChild(window, 0, 1, contPart);
        }
        
        Button finish = this.root.getContainer().nodeCreator().button(8, 2, this.makeFinishButton(new ArrayList<>(), Material.GREEN_TERRACOTTA, 0), BTN_FINISH);
        finish.registerListener(this::finish);
        
        Button duration = this.root.getContainer().nodeCreator().button(7, 2, this.makeDurationButton(0), BTN_DURATION);
        duration.registerExtendedListener((root, instance, info) -> this.modifyDuration(instance, info));
    }
    
    private double calculateTax(List<Triple<ItemStack, Integer, Integer>> parts, int duration) {
        long budget = 0;
        for (Triple<ItemStack, Integer, Integer> part : parts) {
            budget += part.getRight();
        }
        return (double) (budget) * BigAuction.TAX.get(duration);
    }
    
    private void finish(WindowInstance<NodeContainer> instance) {
        List<Triple<ItemStack, Integer, Integer>> parts = this.getParts(instance);
        if (parts.size() <= 0) return;
        
        int duration = (int) instance.getProperty(PROP_DURATION, 0);
        
        double total = 0;
        for (Triple<ItemStack, Integer, Integer> part : parts) {
            total += part.getRight();
        }
        double totalWithTax = this.calculateTax(parts, duration) + total;
    
        RoninAuction.ECONOMY.getAccountAsync(instance.getPlayer().getUniqueId(), RoninAuction.CURRENCY_MAIN)
            .thenAcceptAsync(account -> {
                if (account.getBalance() < totalWithTax) {
                    instance.getPlayer().sendMessage("\u00a7cDu hast nicht genug Geld, um diesen Großauftrag abzugeben.");
                    return;
                }
                
                PlayerContext context = RoninAuction.WINDOW.getContext(instance.getPlayer());
                context.openConfirmation("Großauftrag abgeben", player -> {
                    account.withdraw(totalWithTax);
        
                    List<BigAuctionPart> auctionParts = parts.stream()
                            .map(triple -> new BigAuctionPart(triple.getLeft(), triple.getMiddle(), (double) triple.getRight()))
                            .collect(Collectors.toList());
        
                    BigAuction auction = new BigAuction(
                            player.getUniqueId(),
                            System.currentTimeMillis() + BigAuction.DURATION.get(duration) * 24L * 60L * 60L * 1000L,
                            auctionParts
                    );
                    RoninAuction.getInstance().getBigAuctionCache().add(auction);
                    RoninAuction.getInstance().getBigAuctionCache().save();
        
        
                    player.sendMessage("\u00a7aGroßauftrag erfolgreich erstellt!");
                    Bukkit.getScheduler().runTaskLater(RoninAuction.getInstance(), context::close, 3L);
                }, "Abbrechen!", player -> {}, this.makeFinishButton(parts, Material.BOOK, duration));
            }, RoninAuction.BUKKIT_EXEC);
    }
    
    private List<Triple<ItemStack, Integer, Integer>> getParts(WindowInstance<NodeContainer> instance) {
        List<Triple<ItemStack, Integer, Integer>> parts = new ArrayList<>();
        
        WindowInstanceChild<TabbedContainer> tabbed = instance.searchChild(CONT_TAB);
        for (TabbedContainer.Tab tab : tabbed.child.getContainer().getTabs()) {
            WindowInstanceChild<NodeContainer> actualTab = instance.searchChild(tab.getContainerID());
            ValueChanger amount = actualTab.child.getContainer().getMultiNode(VAL_AMOUNT);
            ValueChanger budget = actualTab.child.getContainer().getMultiNode(VAL_BUDGET);
            
            parts.add(new ImmutableTriple<>(tab.getIcon(), amount.getValue(), budget.getValue()));
        }
        
        return parts;
    }
    
    private void updateFinishButton(WindowInstance<NodeContainer> instance) {
        Button finish = instance.getContainer().getNode(BTN_FINISH);
        finish.setIcon(this.makeFinishButton(this.getParts(instance), Material.GREEN_TERRACOTTA, (int) instance.getProperty(PROP_DURATION, 0)));
    }
    
    private void modifyDuration(WindowInstance<NodeContainer> instance, Button.ClickInfo info) {
        int duration = (int) instance.getProperty(PROP_DURATION, 0);
        if (info.isRight()) {
            duration = Math.max(0, duration - 1);
        } else {
            duration = Math.min(BigAuction.DURATION.size() - 1, duration + 1);
        }
        instance.setProperty(PROP_DURATION, duration);
        
        Button btn = instance.getContainer().getNode(BTN_DURATION);
        btn.setIcon(this.makeDurationButton(duration));
        this.updateFinishButton(instance);
    }
    
    private ItemStack makeFinishButton(List<Triple<ItemStack, Integer, Integer>> parts, Material type, int duration) {
        ItemBuilder builder = RoninAuction.ITEM.builder(type)
                .name("\u00a72Großauftrag erstellen");
    
        for (Triple<ItemStack, Integer, Integer> part : parts) {
            builder.addLore("\u00a77" + part.getMiddle() + "x " + part.getLeft().getType().name()
                + " für " + part.getRight() + " " + RoninAuction.CURRENCY_MAIN.getName());
        }
        double tax = this.calculateTax(parts, duration);
        
        builder.addLore("\u00a7eDauer: \u00a77" + BigAuction.DURATION.get(duration) + " Tage");
        builder.addLore("\u00a7eGebühr: \u00a77" + RoninAuction.CURRENCY_MAIN.formatAmount(tax) + "(" + String.format("%.2f%s", BigAuction.TAX.get(duration) * 100, "%") + ")");
        
        return builder.finish();
    }
    
    private Window<NodeContainer> makePartWindow() {
        Window<NodeContainer> window = RoninAuction.WINDOW.createNodeWindow(9, 1, false);
        window.getContainer().nodeCreator().valueChanger(0, 0, VAL_AMOUNT)
                .min(10).value(10).max(30000)
                .itemStyler((meta, current, amount) -> {
                    meta.setDisplayName("\u00a7eMomentane Anzahl: \u00a77" + current);
                    List<String> lore = new ArrayList<>();
                    lore.add("\u00a7bLinksklick um die Anzahl zu erhöhen (" + amount + ")");
                    lore.add("\u00a7cRechtsklick um die Anzahl zu senken (" + amount + ")");
                    meta.setLore(lore);
                    return meta;
                })
                .addPair(Material.STICK, 10)
                .addPair(Material.BLAZE_ROD, 100)
                .addPair(Material.BLAZE_POWDER, 1000)
                .finish()
        .registerChangeListener((root, instance, value) -> {
            //noinspection unchecked
            this.updateFinishButton((WindowInstance<NodeContainer>) root);
        });
        
        window.getContainer().nodeCreator().valueChanger(4, 0, VAL_BUDGET)
                .min(10).value(10)
                .itemStyler((meta, current, amount) -> {
                    meta.setDisplayName("\u00a7eMomentanes Budget: \u00a77" + current);
                    List<String> lore = new ArrayList<>();
                    lore.add("\u00a7bLinksklick um das Budget zu erhöhen (" + amount + ")");
                    lore.add("\u00a7cRechtsklick um das Budget zu senken (" + amount + ")");
                    meta.setLore(lore);
                    return meta;
                })
                .addPair(Material.IRON_INGOT, 10)
                .addPair(Material.GOLD_INGOT, 100)
                .addPair(Material.DIAMOND, 1000)
                .addPair(Material.DIAMOND_BLOCK, 10000)
                .finish()
        .registerChangeListener((root, instance, value) -> {
            //noinspection unchecked
            this.updateFinishButton((WindowInstance<NodeContainer>) root);
        });
        window.getContainer().hide();
        return window;
    }
    
    private ItemStack makeDurationButton(int duration) {
        return RoninAuction.ITEM.builder(Material.STICK)
                .name("\u00a7eMomentane Auftragsdauer: \u00a77" + BigAuction.DURATION.get(duration) + " Tage")
                .addLore("\u00a7bLinksklick um die Auftragsdauer zu verlängern")
                .addLore("\u00a7cRechtsklick um die Auftragsdauer zu verkürzen")
                .setAmount(BigAuction.DURATION.get(duration))
                .finish();
    }
    
    public void open(Player player) {
        RoninAuction.WINDOW.getContext(player).open(this.root, InventoryType.CHEST, "\u00a72Großauftag erstellen");
    }
}
