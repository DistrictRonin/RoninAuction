package de.ronin.auction.auction.big;

import org.bukkit.inventory.ItemStack;

public class BigAuctionPart {

    private ItemStack type;
    private int amount;
    private double budget;
    
    public BigAuctionPart(ItemStack type, int amount, double budget) {
        this.type = type;
        this.amount = amount;
        this.budget = budget;
    }
    
    public void takeAmount(int amount) {
        double percent = (double) (amount) / this.amount;
        this.budget = this.budget * (1.0D - percent);
        this.amount -= amount;
    }
    
    public ItemStack getType() {
        return this.type;
    }
    
    public int getAmount() {
        return this.amount;
    }
    
    public double getBudget() {
        return this.budget;
    }
}
