package de.ronin.auction.auction.big;

import de.ostylk.baseapi.modules.item.ItemBuilder;
import de.ostylk.baseapi.modules.window.Container;
import de.ostylk.baseapi.modules.window.Window;
import de.ostylk.baseapi.modules.window.WindowInstance;
import de.ostylk.baseapi.modules.window.WindowInstance.WindowInstanceChild;
import de.ostylk.baseapi.modules.window.node.Node;
import de.ostylk.baseapi.modules.window.node.NodeContainer;
import de.ostylk.baseapi.modules.window.node.elements.Button;
import de.ostylk.baseapi.modules.window.storage.FilterResult;
import de.ostylk.baseapi.modules.window.storage.StorageContainer;
import de.ostylk.baseapi.modules.window.storage.event.FilterEvent;
import de.ostylk.baseapi.modules.window.storage.event.FilterEventType;
import de.ronin.auction.ItemUtil;
import de.ronin.auction.RoninAuction;
import de.ronin.post.PostAPI;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class BigAuctionDeliverWindow {

    private static final String CONT_CONTENT = "cont_content";
    private static final String CONT_BUTTONS = "cont_buttons";
    private static final String ND_PART = "nd_part_";
    private static final String BTN_FINISH = "btn_finish";
    
    private BigAuction auction;
    private Window<NodeContainer> root;
    
    public BigAuctionDeliverWindow(BigAuction auction) {
        this.auction = auction;
        this.root = RoninAuction.WINDOW.createNodeWindow(9, 5, true);
        
        Window<StorageContainer> content = RoninAuction.WINDOW.createStorageWindow(9, 4, false);
        content.getContainer().registerFilter(this::filterContent);
        content.getContainer().registerInvalidationListener(this::contentChanged);
        content.registerCloseListener((root, instance, type) -> {
            if (type != Window.CloseType.POP) return;
    
            for (ItemStack is : instance.getContainer().getContents()) {
                if (is == null) continue;
                instance.getPlayer().getInventory().addItem(is);
            }
        });
        this.root.addChild(content, 0, 0, CONT_CONTENT);
        this.root.setChildMoveHandler(CONT_CONTENT);
    
        Window<NodeContainer> buttons = RoninAuction.WINDOW.createNodeWindow(1, 1, false);
        Button finish = buttons.getContainer().nodeCreator().button(0, 0, this.makeFinishButton(new ArrayList<>()), BTN_FINISH);
        finish.registerExtendedListener(this::finishDeliver);
        this.root.addChild(buttons, 8, 4, CONT_BUTTONS);
    
        for (int i = 0; i < this.auction.getParts().size(); i++) {
            BigAuctionPart part = this.auction.getParts().get(i);
            this.root.getContainer().nodeCreator().node(i, 4, this.makeTypeButton(part), ND_PART + i);
        }
    }
    
    private FilterResult filterContent(WindowInstance<? extends Container> root, WindowInstance<StorageContainer> instance, FilterEventType type, FilterEvent event) {
        for (BigAuctionPart part : this.auction.getParts()) {
            if (event.getItem().isSimilar(part.getType())) {
                return FilterResult.ALLOW;
            }
        }
        
        return FilterResult.BLOCK;
    }
    
    private void finishDeliver(WindowInstance<? extends Container> root, WindowInstance<NodeContainer> instance, Button.ClickInfo info) {
        WindowInstanceChild<StorageContainer> contentChild = root.searchChild(CONT_CONTENT);
        //noinspection unchecked
        List<Pair<BigAuctionPart, Integer>> contributions = this.countContributions((WindowInstance<NodeContainer>) root);
        
        if (contributions.size() <= 0) return;
        
        double total = 0.0D;
        for (Pair<BigAuctionPart, Integer> contrib : contributions) {
            double price = contrib.getLeft().getBudget() * ((double) (contrib.getRight()) / contrib.getLeft().getAmount());
            total += price;
        }
    
        final double lambdaFinal = total;
        RoninAuction.ECONOMY.getAccountAsync(instance.getPlayer().getUniqueId(), RoninAuction.CURRENCY_MAIN)
                .thenAcceptAsync(account -> PostAPI.safeDepositMoney(account, lambdaFinal));
    
        for (Pair<BigAuctionPart, Integer> contrib : contributions) {
            int actualAmount = contentChild.child.getContainer().count(contrib.getLeft().getType());
            int diff = actualAmount - contrib.getRight();
            if (diff > 0) {
                instance.getPlayer().getInventory().addItem(ItemUtil.amountToItemStacks(contrib.getLeft().getType(), diff));
            }
        }
        contentChild.child.getContainer().clear();
        
        this.auction.deliverItems(contributions);
        instance.getPlayer().sendMessage("\u00a7aDu hast " + String.format("%.2f", total) + " " + RoninAuction.CURRENCY_MAIN.getName() + " für deinen Beitrag erhalten.");
        Bukkit.getScheduler().runTaskLater(RoninAuction.getInstance(), () -> {
            RoninAuction.WINDOW.getContext(instance.getPlayer()).close();
        }, 1L);
    }
    
    private void contentChanged(WindowInstance<? extends Container> root, WindowInstance<StorageContainer> instance) {
        //noinspection unchecked
        this.updateButtons((WindowInstance<NodeContainer>) root);
    }
    
    private ItemStack makeFinishButton(List<Pair<BigAuctionPart, Integer>> contributions) {
        ItemBuilder builder = RoninAuction.ITEM.builder(Material.GREEN_TERRACOTTA)
                .name("\u00a72Items verkaufen");
    
        for (Pair<BigAuctionPart, Integer> contrib : contributions) {
            double price = contrib.getLeft().getBudget() * ((double) (contrib.getRight()) / contrib.getLeft().getAmount());
            builder = builder.addLore("\u00a7e" + contrib.getRight() + "x \u00a77" + contrib.getLeft().getType().getType().name() + " für "
             + String.format("%.2f", price) + " " + RoninAuction.CURRENCY_MAIN.getName());
        }
        
        return builder.finish();
    }
    
    private ItemStack makeTypeButton(BigAuctionPart part) {
        return RoninAuction.ITEM.builder(part.getType())
                .name("\u00a7e" + part.getAmount() + "x \u00a77" + part.getType().getType().name())
                .addLore("\u00a7eBudget: \u00a77" + String.format("%.2f", part.getBudget()) + " " + RoninAuction.CURRENCY_MAIN.getName())
        .finish();
    }
    
    private List<Pair<BigAuctionPart, Integer>> countContributions(WindowInstance<NodeContainer> instance) {
        List<Pair<BigAuctionPart, Integer>> contributions = new ArrayList<>();
        for (BigAuctionPart part : this.auction.getParts()) {
            WindowInstanceChild<StorageContainer> content = instance.searchChild(CONT_CONTENT);
            int amount = content.child.getContainer().count(part.getType());
            amount = Math.min(amount, part.getAmount());
        
            if (amount > 0) {
                contributions.add(new ImmutablePair<>(part, amount));
            }
        }
        
        return contributions;
    }
    
    
    private void updateButtons(WindowInstance<NodeContainer> root) {
        List<Pair<BigAuctionPart, Integer>> contributions = this.countContributions(root);
    
        WindowInstanceChild<NodeContainer> buttons = root.searchChild(CONT_BUTTONS);
        Button finish = buttons.child.getContainer().getNode(BTN_FINISH);
        finish.setIcon(this.makeFinishButton(contributions));
    }
    
    protected void update() {
        for (int i = 0; i < this.auction.getParts().size(); i++) {
            BigAuctionPart part = this.auction.getParts().get(i);
            Node node = this.root.getContainer().getNode(ND_PART + i);
            node.setIcon(this.makeTypeButton(part));
        }
    }
    
    public void open(Player player) {
        RoninAuction.WINDOW.getContext(player).open(this.root, InventoryType.CHEST, "\u00a72Items verkaufen");
    }
}
