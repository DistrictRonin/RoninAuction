package de.ronin.auction.auction.big;

import de.ostylk.baseapi.modules.config.Config;
import de.ronin.auction.auction.AuctionCache;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BigAuctionCache extends AuctionCache<BigAuction> {
    
    private BigAuctionCreateWindow createWindow;
    
    public BigAuctionCache(Config config) {
        super(config);
        
        this.createWindow = new BigAuctionCreateWindow();
    }
    
    @Override
    public BigAuction loadEntry(String key) {
        UUID player = UUID.fromString(this.config.getString(key + ".player"));
        long deadline = this.config.getLong(key + ".deadline");
        List<BigAuctionPart> parts = new ArrayList<>();
        this.config.getSectionEntries(key + ".parts").forEach(subKey -> {
            ItemStack type = this.config.getItemStack(key + ".parts." + subKey + ".type");
            int amount = this.config.getInt(key + ".parts." + subKey + ".amount");
            double budget = this.config.getDouble(key + ".parts." + subKey + ".budget");
            parts.add(new BigAuctionPart(type, amount, budget));
        });
        return new BigAuction(player, deadline, parts);
    }
    
    @Override
    public void saveEntry(String key, BigAuction auction) {
        this.config.set(key + ".player", auction.getPlayer().toString());
        this.config.set(key + ".deadline", auction.getDeadline());
        this.config.set(key + ".parts", null);
        auction.getParts().forEach(part -> {
            String subKey = UUID.randomUUID().toString();
            this.config.set(key + ".parts." + subKey + ".type", part.getType());
            this.config.set(key + ".parts." + subKey + ".amount", part.getAmount());
            this.config.set(key + ".parts." + subKey + ".budget", part.getBudget());
        });
    }
    
    public BigAuctionCreateWindow getCreateWindow() {
        return this.createWindow;
    }
}
