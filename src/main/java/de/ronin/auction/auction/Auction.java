package de.ronin.auction.auction;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

public abstract class Auction {
    
    private UUID player;
    private long deadline;
    
    public Auction(UUID player, long deadline) {
        this.player = player;
        this.deadline = deadline;
    }
    
    public UUID getPlayer() {
        return this.player;
    }
    
    public long getDeadline() {
        return this.deadline;
    }
    
    public abstract ItemStack makeOverview();
    public abstract void openBuyMenu(Player player);
    public abstract void cancel();
}
