package de.ronin.auction.auction;

import de.ostylk.baseapi.modules.config.Config;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public abstract class AuctionCache<T extends Auction> implements Runnable {

    protected Config config;
    private List<T> auctions;
    
    private AuctionOverview overview;
    
    public AuctionCache(Config config) {
        this.config = config;
        this.auctions = new ArrayList<>();
        
        this.overview = new AuctionOverview();
        
        this.load();
    }
    
    private void load() {
        this.config.getSectionEntries("auctions").forEach(key -> {
            key = "auctions." + key;
            
            T auction = this.loadEntry(key);
            this.overview.addAuction(auction);
            this.auctions.add(auction);
        });
    }
    
    public void save() {
        this.config.set("auctions", null);
        this.auctions.forEach(auction -> this.saveEntry("auctions." + UUID.randomUUID().toString(), auction));
        
        this.config.save();
    }
    
    public void add(T auction) {
        this.auctions.add(auction);
        this.overview.addAuction(auction);
    }
    
    public void remove(T auction) {
        this.auctions.remove(auction);
        this.overview.removeAuction(auction);
    }
    
    @Override
    public void run() {
        List<T> delete = new ArrayList<>();
        for (T auction : this.auctions) {
            if (auction.getDeadline() <= System.currentTimeMillis()) {
                delete.add(auction);
            }
        }
        delete.forEach(Auction::cancel);
    }
    
    public List<T> getPlayerAuctions(Player player) {
        return this.auctions.stream()
                .filter(auction -> auction.getPlayer().equals(player.getUniqueId()))
                .collect(Collectors.toList());
    }
    
    public List<T> getAuctions() {
        return this.auctions;
    }
    
    public AuctionOverview getOverview() {
        return this.overview;
    }
    
    public abstract T loadEntry(String key);
    public abstract void saveEntry(String key, T auction);
}
