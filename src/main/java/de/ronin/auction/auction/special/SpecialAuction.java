package de.ronin.auction.auction.special;

import com.google.common.collect.Lists;
import de.ostylk.baseapi.modules.config.settings.SettingLoad;
import de.ostylk.baseapi.modules.item.ItemBuilder;
import de.ronin.auction.RoninAuction;
import de.ronin.auction.auction.Auction;
import de.ronin.post.PostAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

public class SpecialAuction extends Auction {
    
    @SettingLoad("auction_special.tax")
    public static List<Double> TAX = Lists.newArrayList(0.08, 0.09, 0.1);
    @SettingLoad("auction_special.duration")
    public static List<Integer> DURATION = Lists.newArrayList(7, 14, 21);
    @SettingLoad("auction_special.max_startoffer")
    public static int MAX_STARTOFFER = 100000;
    @SettingLoad("auction_special.max_offersize")
    public static int MAX_OFFERSIZE = 50000;
    
    public static final DateFormat FORMAT = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    
    private ItemStack item;
    private int offerSize;
    
    private UUID offerPlayer;
    private int offer;
    
    private SpecialAuctionBuyWindow buyWindow;
    
    public SpecialAuction(UUID player, long deadline, ItemStack item, UUID offerPlayer, int offer, int offerSize) {
        super(player, deadline);
        this.item = item;
        this.offerPlayer = offerPlayer;
        this.offer = offer;
        this.offerSize = offerSize;
        
        this.buyWindow = new SpecialAuctionBuyWindow(this);
    }
    
    @Override
    public ItemStack makeOverview() {
        String[] names = new String[] { this.getPlayer().toString(), this.offerPlayer.toString() };
        try {
            names = RoninAuction.PLAYER_CACHE.requestPlayerNameAsync(new UUID[] {this.getPlayer(), this.offerPlayer}).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        Date date = new Date(this.getDeadline());
        ItemBuilder overview = RoninAuction.ITEM.builder(this.item)
                .addLore("\u00a7eVerkäufer: \u00a77" + names[0])
                .addLore("\u00a7eGebotgröße: \u00a77" + this.offerSize + " " + RoninAuction.CURRENCY_MAIN.getName())
                .addLore(" ")
                .addLore("\u00a7eHöchstbietender: \u00a77" + names[1])
                .addLore("\u00a7ePreis: \u00a77" + this.offer + " " + RoninAuction.CURRENCY_MAIN.getName())
                .addLore(" ")
                .addLore("\u00a7eEndet am: \u00a77" + FORMAT.format(date) + " Uhr");
        return overview.finish();
    }
    
    @Override
    public void openBuyMenu(Player player) {
        this.buyWindow.open(player);
    }
    
    private void sendMessageIfOnline(UUID player, String message) {
        Player p = Bukkit.getPlayer(player);
        if (p != null) {
            p.sendMessage(message);
        }
    }
    
    public void expired() {
        // Action expired, give the highest bidder the item :)
        PostAPI.sendItems(this.offerPlayer, this.item);
        this.sendMessageIfOnline(this.offerPlayer, "\u00a72Die Sonderauktion ist abgelaufen, du warst der Höchstbietende.");
        this.sendMessageIfOnline(this.offerPlayer, "\u00a72Das Item wurde in dein Postfach gelegt.");
        
        this.item = null;
        
        RoninAuction.getInstance().getSpecialAuctionCache().remove(this);
        RoninAuction.getInstance().getSpecialAuctionCache().save();
    }
    
    @Override
    public void cancel() {
        // Refund the latest bid
        if (!this.offerPlayer.equals(this.getPlayer())) {
            final int offer = this.offer;
            RoninAuction.ECONOMY.getAccountAsync(this.offerPlayer, RoninAuction.CURRENCY_MAIN)
                    .thenAcceptAsync(refund -> {
                        PostAPI.safeDepositMoney(refund, offer);
                    });
        }
        if (this.item == null) return;
        
        PostAPI.sendItems(this.getPlayer(), this.item);
        this.item = null;
        
        RoninAuction.getInstance().getSpecialAuctionCache().remove(this);
        RoninAuction.getInstance().getSpecialAuctionCache().save();
        
        //TODO: offline messages
    }
    
    public void setOffer(UUID player, int offer) {
        this.offerPlayer = player;
        this.offer = offer;
        RoninAuction.getInstance().getSpecialAuctionCache().save();
    }
    
    public ItemStack getItem() {
        return this.item;
    }
    
    public int getOfferSize() {
        return this.offerSize;
    }
    
    public UUID getOfferPlayer() {
        return this.offerPlayer;
    }
    
    public int getOffer() {
        return this.offer;
    }
}
