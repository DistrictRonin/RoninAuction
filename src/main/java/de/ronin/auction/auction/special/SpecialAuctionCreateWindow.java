package de.ronin.auction.auction.special;

import de.ostylk.baseapi.modules.economy.Account;
import de.ostylk.baseapi.modules.window.ModuleWindow;
import de.ostylk.baseapi.modules.window.Window;
import de.ostylk.baseapi.modules.window.WindowInstance;
import de.ostylk.baseapi.modules.window.node.NodeContainer;
import de.ostylk.baseapi.modules.window.node.elements.Button;
import de.ostylk.baseapi.modules.window.node.elements.ValueChanger;
import de.ostylk.baseapi.modules.window.player.PlayerContext;
import de.ostylk.baseapi.modules.window.storage.StorageContainer;
import de.ronin.auction.RoninAuction;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class SpecialAuctionCreateWindow {
    
    private Window<NodeContainer> root;
    
    private static final String CONT_ITEM = "cont_item";
    
    private static final String VAL_STARTOFFER = "val_startoffer";
    private static final String VAL_OFFERSIZE = "val_offersize";
    
    private static final String BTN_DURATION = "btn_duration";
    private static final String PROP_DURATION = "duration";
    
    private static final String BTN_FINISH = "btn_finish";
    
    public SpecialAuctionCreateWindow() {
        ModuleWindow win = RoninAuction.WINDOW;
        
        this.root = win.createNodeWindow(9, 3, false);
        Window<StorageContainer> item = win.createStorageWindow(1, 1, false);
        item.registerCloseListener((root, instance, type) -> {
            if (type != Window.CloseType.POP) return;
            if (instance.getContainer().get(0) == null) return;
            instance.getPlayer().getInventory().addItem(instance.getContainer().get(0));
        });
        this.root.addChild(item, 4, 0, CONT_ITEM);
        this.root.setChildMoveHandler(CONT_ITEM);
        
        this.root.getContainer().nodeCreator().valueChanger(0, 1, VAL_STARTOFFER)
                .min(1000).value(1000).max(SpecialAuction.MAX_STARTOFFER)
                .itemStyler((meta, current, amount) -> {
                    meta.setDisplayName("\u00a7eMomentanes Startgebot: \u00a77" + current + " " + RoninAuction.CURRENCY_MAIN.getName());
                    List<String> lore = new ArrayList<>();
                    lore.add("\u00a7bLinksklick um das Startgebot zu erhöhen (" + amount + ")");
                    lore.add("\u00a7cRechtsklick um das Startgebot zu senken (" + amount + ")");
                    meta.setLore(lore);
                    return meta;
                })
                .addPair(Material.IRON_INGOT,10)
                .addPair(Material.GOLD_INGOT, 100)
                .addPair(Material.DIAMOND, 1000)
                .addPair(Material.DIAMOND_BLOCK, 10000)
                .finish()
         .registerChangeListener((root, instance, value) -> this.updateButtons(instance));
        
        this.root.getContainer().nodeCreator().valueChanger(5, 1, VAL_OFFERSIZE)
                .min(100).value(100).max(SpecialAuction.MAX_OFFERSIZE)
                .itemStyler((meta, current, amount) -> {
                    meta.setDisplayName("\u00a7eMomentane Gebotgröße: \u00a77" + current + " " + RoninAuction.CURRENCY_MAIN.getName());
                    List<String> lore = new ArrayList<>();
                    lore.add("\u00a7bLinksklick um die Gebotgröße zu erhöhen (" + amount + ")");
                    lore.add("\u00a7cRechtsklick um die Gebotgröße zu senken (" + amount + ")");
                    meta.setLore(lore);
                    return meta;
                })
                .addPair(Material.IRON_INGOT, 10)
                .addPair(Material.GOLD_INGOT, 100)
                .addPair(Material.DIAMOND, 1000)
                .addPair(Material.DIAMOND_BLOCK, 10000)
                .finish()
         .registerChangeListener((root, instance, value) -> this.updateButtons(instance));
        
        Button duration = this.root.getContainer().nodeCreator().button(4, 2, this.makeDurationButton(0), BTN_DURATION);
        duration.registerExtendedListener((root, instance, info) -> this.modifyDuration(instance, info));
        
        Button finish = this.root.getContainer().nodeCreator().button(8, 2, this.makeFinishButton(1000, 100, 0), BTN_FINISH);
        finish.registerListener(this::finishAuction);
    }
    
    private void finishAuction(WindowInstance<NodeContainer> instance) {
        WindowInstance.WindowInstanceChild<StorageContainer> itemChild = instance.searchChild(CONT_ITEM);
        StorageContainer item = itemChild.child.getContainer();
        if (item.get(0) == null || item.get(0).getType() == Material.AIR || item.get(0).getAmount() <= 0) return;
        
        ValueChanger startoffer = instance.getContainer().getMultiNode(VAL_STARTOFFER);
        ValueChanger offersize = instance.getContainer().getMultiNode(VAL_OFFERSIZE);
        int duration = (int) instance.getProperty(PROP_DURATION, 0);
        double tax = SpecialAuction.TAX.get(duration);
        double actualTax = startoffer.getValue() * tax;
    
        PlayerContext context = RoninAuction.WINDOW.getContext(instance.getPlayer());
        
        ItemStack info = RoninAuction.ITEM.builder(item.get(0))
                .addLore("\u00a77Startgebot: " + startoffer.getValue() + " " + RoninAuction.CURRENCY_MAIN.getName())
                .addLore("\u00a77Gebotgröße: " + offersize.getValue() + " " + RoninAuction.CURRENCY_MAIN.getName())
                .addLore("\u00a77Auktionsdauer: " + SpecialAuction.DURATION.get(duration) + " Tage")
                .addLore("\u00a77Gebühr: " + String.format("%.2f %s (%.2f%s)", actualTax, RoninAuction.CURRENCY_MAIN.getName(), tax * 100, "%"))
                .finish();
        context.openConfirmation("Sonderauktion erstellen", player -> {
            SpecialAuction auction = new SpecialAuction(
                    instance.getPlayer().getUniqueId(),
                    System.currentTimeMillis() + SpecialAuction.DURATION.get(duration) * 24L * 60L * 60L * 1000L,
                    item.get(0).clone(),
                    instance.getPlayer().getUniqueId(),
                    startoffer.getValue(),
                    offersize.getValue()
            );
            if (this.finaliseAuction(auction, player, actualTax)) {
                item.clear();
                Bukkit.getScheduler().runTaskLater(RoninAuction.getInstance(), context::close, 3L);
            }
        }, "Abbrechen!", player -> {}, info);
    }
    
    private boolean finaliseAuction(SpecialAuction auction, Player player, double tax) {
        Account account;
        try {
            account = RoninAuction.ECONOMY.getAccountAsync(auction.getPlayer(), RoninAuction.CURRENCY_MAIN).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return false;
        }
        switch (account.withdraw(tax)) {
            case NOT_ENOUGH_MONEY:
                player.sendMessage("\u00a7cDu hast nicht genug Geld, um die Gebühren zu bezahlen.");
                player.sendMessage("\u00a7cDu könntest das Startgebot senken, um niedrigere Gebühren zu zahlen.");
                return false;
            case SUCCESS:
                RoninAuction.getInstance().getSpecialAuctionCache().add(auction);
                RoninAuction.getInstance().getSpecialAuctionCache().save();
                player.sendMessage("\u00a7aSonderauktion erfolgreich erstellt!");
        }
        return true;
    }
    
    private void modifyDuration(WindowInstance<NodeContainer> instance, Button.ClickInfo info) {
        int duration = (int) instance.getProperty(PROP_DURATION, 0);
        if (info.isRight()) {
            duration = Math.max(0, duration - 1);
        } else {
            duration = Math.min(SpecialAuction.DURATION.size() - 1, duration + 1);
        }
        instance.setProperty(PROP_DURATION, duration);
        
        this.updateButtons(instance);
    }
    
    private void updateButtons(WindowInstance<NodeContainer> instance) {
        ValueChanger startoffer = instance.getContainer().getMultiNode(VAL_STARTOFFER);
        ValueChanger offersize = instance.getContainer().getMultiNode(VAL_OFFERSIZE);
        int duration = (int) instance.getProperty(PROP_DURATION, 0);
        
        Button durationBtn = instance.getContainer().getNode(BTN_DURATION);
        durationBtn.setIcon(this.makeDurationButton(duration));
        
        Button finishBtn = instance.getContainer().getNode(BTN_FINISH);
        finishBtn.setIcon(this.makeFinishButton(startoffer.getValue(), offersize.getValue(), duration));
    }
    
    private ItemStack makeDurationButton(int duration) {
        return RoninAuction.ITEM.builder(Material.STICK)
                .name("\u00a7eMomentane Auktionsdauer: \u00a77" + SpecialAuction.DURATION.get(duration) + " Tage")
                .addLore("\u00a7bLinksklick um die Auktionsdauer zu verlängern")
                .addLore("\u00a7cRechtsklick um die Auktionsdauer zu verkürzen")
                .setAmount(SpecialAuction.DURATION.get(duration))
                .finish();
    }
    
    private ItemStack makeFinishButton(int startoffer, int offersize, int duration) {
        return RoninAuction.ITEM.builder(Material.GREEN_TERRACOTTA)
                .name("\u00a72Sonderauktion erstellen")
                .addLore("\u00a77Startgebot: " + startoffer + " " + RoninAuction.CURRENCY_MAIN.getName())
                .addLore("\u00a77Gebotgröße: " + offersize + " " + RoninAuction.CURRENCY_MAIN.getName())
                .addLore("\u00a77Auktionsdauer: " + SpecialAuction.DURATION.get(duration) + " Tage")
                .addLore("\u00a77Gebühr: " + String.format("%.2f%s", SpecialAuction.TAX.get(duration) * 100, "%"))
                .finish();
    }
    
    public void open(Player player) {
        RoninAuction.WINDOW.getContext(player).open(this.root, InventoryType.CHEST, "\u00a72Sonderauktion erstellen");
    }
}
