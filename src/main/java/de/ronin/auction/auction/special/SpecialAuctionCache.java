package de.ronin.auction.auction.special;

import de.ostylk.baseapi.modules.config.Config;
import de.ronin.auction.auction.AuctionCache;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class SpecialAuctionCache extends AuctionCache<SpecialAuction> {
    
    private SpecialAuctionCreateWindow createWindow;
    
    public SpecialAuctionCache(Config config) {
        super(config);
        this.createWindow = new SpecialAuctionCreateWindow();
    }
    
    @Override
    public void run() {
        List<SpecialAuction> expired = new ArrayList<>();
        for (SpecialAuction auction : this.getAuctions()) {
            if (auction.getDeadline() <= System.currentTimeMillis()) {
                expired.add(auction);
            }
        }
        expired.forEach(SpecialAuction::expired);
    }
    
    @Override
    public SpecialAuction loadEntry(String key) {
        UUID player = UUID.fromString(this.config.getString(key + ".player"));
        long deadline = this.config.getLong(key + ".deadline");
        ItemStack item = this.config.getItemStack(key + ".item");
        int offerSize = this.config.getInt(key + ".offerSize");
        UUID offerPlayer = UUID.fromString(this.config.getString(key + ".offerPlayer"));
        int offer = this.config.getInt(key + ".offer");
        return new SpecialAuction(player, deadline, item, offerPlayer, offer, offerSize);
    }
    
    @Override
    public void saveEntry(String key, SpecialAuction auction) {
        this.config.set(key + ".player", auction.getPlayer().toString());
        this.config.set(key + ".deadline", auction.getDeadline());
        this.config.set(key + ".item", auction.getItem());
        this.config.set(key + ".offerSize", auction.getOfferSize());
        this.config.set(key + ".offerPlayer", auction.getOfferPlayer().toString());
        this.config.set(key + ".offer", auction.getOffer());
    }
    
    public SpecialAuctionCreateWindow getCreateWindow() {
        return this.createWindow;
    }
}
