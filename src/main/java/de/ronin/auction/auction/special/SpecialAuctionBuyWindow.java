package de.ronin.auction.auction.special;

import de.ostylk.baseapi.modules.economy.Account;
import de.ostylk.baseapi.modules.window.Window;
import de.ostylk.baseapi.modules.window.WindowInstance;
import de.ostylk.baseapi.modules.window.node.Node;
import de.ostylk.baseapi.modules.window.node.NodeContainer;
import de.ostylk.baseapi.modules.window.node.elements.Button;
import de.ronin.auction.RoninAuction;
import de.ronin.post.PostAPI;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

public class SpecialAuctionBuyWindow {

    private SpecialAuction auction;
    
    private Window<NodeContainer> root;
    private Node auctionOverview;
    private Button auctionBid;
    
    public SpecialAuctionBuyWindow(SpecialAuction auction) {
        this.auction = auction;
        
        this.root = RoninAuction.WINDOW.createNodeWindow(9, 1, true);
        this.auctionOverview = this.root.getContainer().nodeCreator().node(1, 0, this.auction.makeOverview());
        this.auctionBid = this.root.getContainer().nodeCreator().button(7, 0, this.makeBidButton());
        this.auctionBid.registerListener(this::bid);
    }
    
    private void sendMessageIfOnline(UUID player, String message) {
        Player p = Bukkit.getPlayer(player);
        if (p != null) {
            p.sendMessage(message);
        }
    }
    
    private void bid(WindowInstance<NodeContainer> instance) {
        if (instance.getPlayer().getUniqueId().equals(this.auction.getPlayer())) return;
    
        CompletableFuture<Account> newBidderF = RoninAuction.ECONOMY.getAccountAsync(instance.getPlayer().getUniqueId(), RoninAuction.CURRENCY_MAIN);
        CompletableFuture<Account> oldBidderF = RoninAuction.ECONOMY.getAccountAsync(this.auction.getOfferPlayer(), RoninAuction.CURRENCY_MAIN);
        newBidderF.thenAcceptBothAsync(oldBidderF, (newBidder, oldBidder) -> {
            int newOffer = this.auction.getOffer() + this.auction.getOfferSize();
            if (newBidder.getBalance() < newOffer) {
                instance.getPlayer().sendMessage("\u00a7cDu hast nicht genug Geld, um dieses Gebot abzugeben.");
                return;
            }
    
            if (!oldBidder.getOwner().equals(this.auction.getPlayer())) {
                PostAPI.safeDepositMoney(oldBidder, this.auction.getOffer());
                this.sendMessageIfOnline(oldBidder.getOwner(), "\u00a7eDu wurdest bei einer Sonderauktion überboten. Du hast dein Geld zurückerhalten.");
            }
            
            newBidder.withdraw(newOffer);
            this.auction.setOffer(instance.getPlayer().getUniqueId(), newOffer);
            
            this.update();
        });
    }
    
    private ItemStack makeBidButton() {
        int newOffer = this.auction.getOffer() + this.auction.getOfferSize();
        return RoninAuction.ITEM.builder(Material.YELLOW_TERRACOTTA)
                .name("\u00a72Auf " + newOffer + " " + RoninAuction.CURRENCY_MAIN.getName() + " bieten")
                .addLore("\u00a7eMomentanes Gebot: \u00a77" + this.auction.getOffer() + " " + RoninAuction.CURRENCY_MAIN.getName())
                .addLore("\u00a7eLäuft bis zum: \u00a77" + SpecialAuction.FORMAT.format(new Date(this.auction.getDeadline())) + " Uhr")
                .finish();
    }
    
    protected void update() {
        this.auctionOverview.setIcon(this.auction.makeOverview());
        this.auctionBid.setIcon(this.makeBidButton());
    }
    
    protected void open(Player player) {
        RoninAuction.WINDOW.getContext(player).open(this.root, InventoryType.CHEST, "\u00a72Bieten");
    }
}
