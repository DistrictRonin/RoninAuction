package de.ronin.auction.auction;

import de.ostylk.baseapi.modules.window.Window;
import de.ostylk.baseapi.modules.window.WindowInstance;
import de.ostylk.baseapi.modules.window.misc.MapContainer;
import de.ostylk.baseapi.modules.window.node.NodeContainer;
import de.ronin.auction.RoninAuction;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;

import java.util.HashSet;
import java.util.Set;

public class AuctionOverview {
    
    private Window<NodeContainer> root;
    private Window<MapContainer<Auction>> personalAuctions;
    private Window<MapContainer<Auction>> allAuctions;
    
    private Set<WindowInstance<NodeContainer>> playerInventories;
    
    private AuctionCancel cancelWindow;
    
    public static final String CONTAINER_PERSONAL = "map_personal";
    
    public AuctionOverview() {
        this.root = RoninAuction.WINDOW.createNodeWindow(9, 6, true);
        this.root.registerOpenListener(instance -> {
            this.rebuildPersonalAuctions(instance, instance.getPlayer());
        });
        
        this.personalAuctions = RoninAuction.WINDOW.createMapWindow(9, 1, false, Auction.class, Auction::makeOverview);
        this.personalAuctions.getContainer().registerClickListener(((instance, value, index) -> {
            this.cancelWindow.open(instance.getPlayer(), value);
        }));
        this.root.addChild(this.personalAuctions, 0, 1, CONTAINER_PERSONAL);
        
        this.allAuctions = RoninAuction.WINDOW.createMapWindow(9, 3, true, Auction.class, Auction::makeOverview);
        this.allAuctions.getContainer().setPageBehavior(MapContainer.MapPageBehavior.ALWAYS);
        this.allAuctions.getContainer().registerClickListener((instance, value, index) -> {
            /*if (value.getPlayer().equals(instance.getPlayer().getUniqueId())) {
                this.cancelWindow.open(instance.getPlayer(), value);
            } else {
                //TODO: open buy window
            }*/
            value.openBuyMenu(instance.getPlayer());
        });
        this.root.addChild(this.allAuctions, 0, 3);
        
        this.root.getContainer().nodeCreator().node(0, 0,
                RoninAuction.ITEM.builder(Material.PAPER)
                    .name("\u00a7eDeine Auktionen")
                .finish());
        
        this.root.getContainer().nodeCreator().node(0, 2,
                RoninAuction.ITEM.builder(Material.PAPER)
                    .name("\u00a7eAlle Auktionen")
                .finish());
        
        this.playerInventories = new HashSet<>();
        this.cancelWindow = new AuctionCancel();
        this.root.registerOpenListener(playerInventories::add);
        this.root.registerCloseListener((root, instance, type) -> playerInventories.remove(instance));
    }
    
    public void addAuction(Auction auction) {
        this.allAuctions.getContainer().add(auction);
        this.playerInventories.forEach(instance -> {
            this.rebuildPersonalAuctions(instance, instance.getPlayer());
        });
    }
    
    public void removeAuction(Auction auction) {
        this.allAuctions.getContainer().remove(auction);
        this.playerInventories.forEach(instance -> {
            this.rebuildPersonalAuctions(instance, instance.getPlayer());
        });
    }
    
    private void rebuildPersonalAuctions(WindowInstance<NodeContainer> instance, Player player) {
        WindowInstance<MapContainer<Auction>> personal = instance.<MapContainer<Auction>>searchChild(CONTAINER_PERSONAL).child;
        personal.getContainer().clear();
        this.allAuctions.getContainer().getContents().forEach(auction -> {
            if (auction.getPlayer().equals(player.getUniqueId())) {
                personal.getContainer().add(auction);
            }
        });
        RoninAuction.WINDOW.getContext(player).triggerUpdate();
    }
    
    public void open(Player player) {
        RoninAuction.WINDOW.getContext(player).open(this.root, InventoryType.CHEST, "\u00a72Übersicht");
    }
}
