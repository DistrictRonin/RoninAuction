package de.ronin.auction.listener;

import de.ostylk.baseapi.modules.window.Window;
import de.ostylk.baseapi.modules.window.node.NodeContainer;
import de.ostylk.baseapi.modules.window.node.NodeCreator;
import de.ostylk.baseapi.modules.window.node.elements.Button;
import de.ronin.auction.RoninAuction;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.inventory.EquipmentSlot;

public class AuctioneerMainMenu implements Listener {

    private Window<NodeContainer> mainMenu;
    
    public AuctioneerMainMenu() {
        this.mainMenu = RoninAuction.WINDOW.createNodeWindow(9, 2, true);
        NodeCreator nodeCreator = this.mainMenu.getContainer().nodeCreator();
        this.createAuctionButton(nodeCreator);
        this.createSpecialButton(nodeCreator);
        this.createBigAuctionButton(nodeCreator);
    }
    
    private void createAuctionButton(NodeCreator nodeCreator) {
        Button auctionCheck = nodeCreator.button(1, 0,
                RoninAuction.ITEM.builder(Material.BOOK)
                    .name("\u00a7eAuktionen einsehen")
                .finish()
        );
        auctionCheck.registerListener(instance -> {
            RoninAuction.getInstance().getNormalAuctionCache().getOverview().open(instance.getPlayer());
        });
        
        Button auctionCreate = nodeCreator.button(1, 1,
                RoninAuction.ITEM.builder(Material.WRITABLE_BOOK)
                    .name("\u00a7eAuktion erstellen")
                .finish()
        );
        auctionCreate.registerListener(instance -> {
            if (RoninAuction.getInstance().getNormalAuctionCache().getPlayerAuctions(instance.getPlayer()).size() >= 9) {
                instance.getPlayer().sendMessage("\u00a7cDu kannst nichtmehr als 9 Auktionen zur selben Zeit haben.");
            } else {
                RoninAuction.getInstance().getNormalAuctionCache().getCreateWindow().open(instance.getPlayer());
            }
        });
    }
    
    private void createSpecialButton(NodeCreator nodeCreator) {
        Button specialCheck = nodeCreator.button(4, 0,
                RoninAuction.ITEM.builder(Material.BOOK)
                    .name("\u00a7eSonderauktionen einsehen")
                .finish()
        );
        specialCheck.registerListener(instance -> {
            RoninAuction.getInstance().getSpecialAuctionCache().getOverview().open(instance.getPlayer());
        });
        
        Button specialCreate = nodeCreator.button(4, 1,
                RoninAuction.ITEM.builder(Material.WRITABLE_BOOK)
                    .name("\u00a7eSonderauktion erstellen")
                .finish()
        );
        specialCreate.registerListener(instance -> {
            if (RoninAuction.getInstance().getSpecialAuctionCache().getPlayerAuctions(instance.getPlayer()).size() >= 9) {
                instance.getPlayer().sendMessage("\u00a7cDu kannst nichtmehr als 9 Sonderauktionen zur selben Zeit haben.");
            } else {
                RoninAuction.getInstance().getSpecialAuctionCache().getCreateWindow().open(instance.getPlayer());
            }
        });
    }
    
    private void createBigAuctionButton(NodeCreator nodeCreator) {
        Button bigAuctionCheck = nodeCreator.button(7, 0,
                RoninAuction.ITEM.builder(Material.BOOK)
                    .name("\u00a7eGroßaufträge einsehen")
                .finish()
        );
        bigAuctionCheck.registerListener(instance -> {
            RoninAuction.getInstance().getBigAuctionCache().getOverview().open(instance.getPlayer());
        });
        
        Button bigAuctionCreate = nodeCreator.button(7, 1,
                RoninAuction.ITEM.builder(Material.WRITABLE_BOOK)
                    .name("\u00a7eGroßauftrag erstellen")
                .finish()
        );
        bigAuctionCreate.registerListener(instance -> {
            if (RoninAuction.getInstance().getBigAuctionCache().getPlayerAuctions(instance.getPlayer()).size() >= 9) {
                instance.getPlayer().sendMessage("\u00a7cDu kannst nichtmehr als 9 Großaufträge zur selben Zeit haben.");
            } else {
                RoninAuction.getInstance().getBigAuctionCache().getCreateWindow().open(instance.getPlayer());
            }
        });
    }
    
    @EventHandler
    public void onInteract(PlayerInteractAtEntityEvent e) {
        if (e.getHand() != EquipmentSlot.HAND) return;
        if (RoninAuction.getInstance().getNPCCache().isNPC(e.getRightClicked())) {
            RoninAuction.WINDOW.getContext(e.getPlayer()).open(this.mainMenu, InventoryType.CHEST, "\u00a72Auktionator");
        }
    }
}
