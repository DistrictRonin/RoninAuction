package de.ronin.auction;

import org.bukkit.inventory.ItemStack;

public class ItemUtil {

    public static ItemStack[] amountToItemStacks(ItemStack type, int amount) {
        int fullStacks = amount / 64;
        int remainder = amount % 64;
        
        ItemStack[] items = new ItemStack[fullStacks + (remainder == 0 ? 0 : 1)];
        for (int i = 0; i < fullStacks; i++) {
            items[i] = type.clone();
            items[i].setAmount(64);
        }
        
        if (remainder != 0) {
            items[items.length - 1] = type.clone();
            items[items.length - 1].setAmount(remainder);
        }
        
        return items;
    }
    
    public static String nameQuantity(long amount) {
        int stacks = (int) (amount / 64);
        int remainder = (int) (amount % 64);
        
        if (stacks <= 0) {
            return String.valueOf(remainder);
        } else if (stacks == 1) {
            return "1 Stack" + nameSingleQuantity(remainder);
        } else {
            return stacks + " Stacks" + nameSingleQuantity(remainder);
        }
    }
    
    private static String nameSingleQuantity(int amount) {
        if (amount <= 0) {
            return "";
        } else {
            return " + " + amount;
        }
    }
}
